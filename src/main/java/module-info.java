module tsp.main {
    requires java.net.http;
    requires javafx.controls;
    requires javafx.fxml;
    requires io.jenetics.base;

    exports me.factorify;
    opens me.factorify to javafx.fxml;
}
