package me.factorify;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import me.factorify.data.GenerateData;
import me.factorify.data.Location;
import me.factorify.data.TspInput;
import me.factorify.data.TspSolution;
import me.factorify.solvers.*;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.*;

public class Controller implements Initializable {
    private static final int NODE_RADIUS = 3;

    @FXML
    public TextField locationsInput;
    @FXML
    private Label distance;
    @FXML
    private Label elapsedTime;
    @FXML
    private ChoiceBox<String> algorithms;
    @FXML
    private Button startButton;
    @FXML
    private Canvas canvas;
    private GraphicsContext graphicsContext;

    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(2, runnable -> {
        var thread = new Thread(runnable);
        thread.setDaemon(true);
        return thread;
    });
    private TspInput input;
    private int elapsedSeconds;
    private ScheduledFuture<?> timerTask;
    private TspSolver solver;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.graphicsContext = canvas.getGraphicsContext2D();
        this.algorithms.setItems(FXCollections.observableArrayList(
                "Naive",
                "Greedy",
                "Genetic",
                "Genetic II",
                "Branch & bound"
        ));
        this.algorithms.setValue("Greedy");
    }

    @FXML
    public void generateData() {
        stop();
        int locations = Integer.parseInt(locationsInput.getText());
        executor.execute(() -> {
            input = new GenerateData(locations, canvas.getWidth(), canvas.getHeight()).run();
            redrawSolution(null);
        });
    }

    public void start() {
        this.elapsedSeconds = 0;
        this.timerTask = executor.scheduleAtFixedRate(this::drawTimer, 1, 1, TimeUnit.SECONDS);
        this.solver = chooseSolver();
        executor.execute(() -> {
            this.solver
                    .setNewSolution(this::redrawSolution)
                    .setFinish(this::stop)
                    .run();
        });
    }

    private TspSolver chooseSolver() {
        switch (this.algorithms.getValue()) {
            case "Naive":
                return new NaiveSolver(input);
            case "Genetic":
                return new GASolver(input);
            case "Genetic II":
                return new GA2Solver(input);
            case "Branch & bound":
                return new BranchAndBoundSolver(input);
            default:
                return new GreedySolver(input);
        }
    }

    public void stop() {
        if (timerTask != null) {
            timerTask.cancel(false);
            timerTask = null;
        }
        if (this.solver != null) {
            this.solver.stop();
            this.solver = null;
        }
        Platform.runLater(() -> startButton.setText("Start"));
    }

    private void redrawSolution(TspSolution solution) {
        Platform.runLater(() -> {
            canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

            graphicsContext.setFill(Color.RED);
            for (Location location : input.getLocations()) {
                drawNode(location);
            }

            if (solution != null) {
                graphicsContext.setStroke(Color.BLACK);
                graphicsContext.beginPath();
                graphicsContext.moveTo(input.getStartLocation().getX(), input.getStartLocation().getY());
                for (Location location : solution.getPath()) {
                    graphicsContext.lineTo(location.getX(), location.getY());
                }
                graphicsContext.lineTo(input.getStartLocation().getX(), input.getStartLocation().getY());
                graphicsContext.stroke();
                double distance = TspSolver.calculateDistance(input.getStartLocation(), solution.getPath());
                this.distance.setText(String.valueOf((int) distance));
            }
        });
    }

    private void drawNode(Location location) {
        graphicsContext.fillOval(location.getX() - NODE_RADIUS, location.getY() - NODE_RADIUS, NODE_RADIUS, NODE_RADIUS);
    }

    private void drawTimer() {
        elapsedSeconds++;
        Platform.runLater(() -> elapsedTime.setText(elapsedSeconds + " s"));
    }
}
