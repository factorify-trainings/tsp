package me.factorify.data;

import me.factorify.solvers.TspSolver;

import java.util.ArrayList;
import java.util.List;

public class GenerateData {
    private final int numberOfLocations;
    private final int width;
    private final int height;

    private final List<Location> locations = new ArrayList<>();
    private final double[][] distanceMatrix;

    public GenerateData(int numberOfLocations, double width, double height) {
        this.numberOfLocations = numberOfLocations;
        this.width = (int) width;
        this.height = (int) height;
        this.distanceMatrix = new double[numberOfLocations][numberOfLocations];
    }

    public TspInput run() {
        for (int i = 0; i < numberOfLocations; i++) {
            this.locations.add(generateLocation(i));
        }

        for (int i = 0; i < locations.size(); i++) {
            for (int j = i + 1; j < locations.size(); j++) {
                double distance = TspSolver.calculateDistance(locations.get(i), locations.get(j));
                distanceMatrix[i][j] = distance;
                distanceMatrix[j][i] = distance;
            }
        }

        return new TspInput(generateLocation(-1), locations, distanceMatrix);
    }

    private Location generateLocation(int i) {
        return new Location(i, (int) Math.round(Math.random() * width), (int) Math.round(Math.random() * height));
    }

}
