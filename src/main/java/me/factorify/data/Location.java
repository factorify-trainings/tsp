package me.factorify.data;

public class Location {
    private final int id;
    private final int x;
    private final int y;

    public Location(int id, int x, int y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public int getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
