package me.factorify.data;

import java.util.List;

public class TspSolution {
   private final List<Location> path;

    public TspSolution(List<Location> path) {
        this.path = path;
    }

    public List<Location> getPath() {
        return path;
    }
}
