package me.factorify.data;

import java.util.List;

public class TspInput {
    private final Location startLocation;
    private final List<Location> locations;
    private final double[][] distanceMatrix;

    public TspInput(Location startLocation, List<Location> locations, double[][] distanceMatrix) {
        this.startLocation = startLocation;
        this.locations = locations;
        this.distanceMatrix = distanceMatrix;
    }

    public Location getStartLocation() {
        return startLocation;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public double getDistance(Location location, Location otherLocation) {
        return distanceMatrix[location.getId()][otherLocation.getId()];
    }
}
