package me.factorify.solvers;

import me.factorify.data.Location;
import me.factorify.data.TspInput;
import me.factorify.data.TspSolution;

import java.util.List;
import java.util.function.Consumer;

public abstract class TspSolver {
    protected final TspInput input;
    private Runnable finish;
    private Consumer<TspSolution> newSolution;

    public TspSolver(TspInput input) {
        this.input = input;
    }

    public static double calculateDistance(Location location, Location otherLocation) {
        int x = Math.abs(location.getX() - otherLocation.getX());
        int y = Math.abs(location.getY() - otherLocation.getY());
        return Math.sqrt(x * x + y * y);
    }

    public static double calculateDistance(Location startLocation, Iterable<Location> locations) {
        Location previousLocation = startLocation;
        double distance = 0;
        for (Location location : locations) {
            if (previousLocation != null) {
                distance += calculateDistance(previousLocation, location);
            }
            previousLocation = location;
        }
        distance += calculateDistance(previousLocation, startLocation);
        return distance;
    }

    public TspSolver setNewSolution(Consumer<TspSolution> newSolution) {
        this.newSolution = newSolution;
        return this;
    }

    public TspSolver setFinish(Runnable finish) {
        this.finish = finish;
        return this;
    }

    public void stop() {
    }

    public abstract void run();

    protected void finished() {
        finish.run();
    }

    protected void newSolution(TspSolution solution) {
        newSolution.accept(solution);
    }
}
