package me.factorify.solvers;

import me.factorify.data.TspInput;
import me.factorify.data.TspSolution;

public class NaiveSolver extends TspSolver {

    public NaiveSolver(TspInput input) {
        super(input);
    }

    @Override
    public void run() {
        newSolution(new TspSolution(input.getLocations()));
        finished();
    }
}
